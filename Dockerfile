FROM golang:latest

LABEL maintainer="M.Chernov, <kushkaftar@gmail.com>"

WORKDIR /app

COPY go.mod .
#COPY go.sum .

RUN go mod download

COPY . .

RUN go build -o main ./cmd

CMD ["./main"]
