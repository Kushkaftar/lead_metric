package config

type Config struct {
	API API `yaml:"api"`
}

type API struct {
	Port      string `yaml:"port"`
	DebugMode bool   `yaml:"debugMode"`
}
