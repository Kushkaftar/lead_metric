package api

import (
	"go.uber.org/zap"
	"lead_metric/internal/server"
	"lead_metric/internal/service"
	"lead_metric/pkg/config"
)

type Handler struct {
	s    *service.ApiMethod
	logg *zap.Logger
}

func newHandler(s *service.ApiMethod, logg *zap.Logger) *Handler {
	return &Handler{
		s:    s,
		logg: logg,
	}
}

func Start(c *config.Config, s *service.ApiMethod, logg *zap.Logger) {
	h := newHandler(s, logg)
	srv := new(server.Server)

	if err := srv.Run(c.API.Port, h.InitRoutes(c.API.DebugMode)); err != nil {
		logg.Fatal("http server not running, error", zap.Error(err))
	}
}
