package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func (h *Handler) InitRoutes(debugMode bool) *gin.Engine {
	if !debugMode {
		gin.SetMode(gin.ReleaseMode)
	}

	router := gin.New()
	router.Use(cors.Default())

	api := router.Group("/api")
	{
		domain := api.Group("/domain")
		{
			domain.GET("/", h.domain)
			domain.POST("/set-status", h.setStatus)
		}
	}
	return router
}
