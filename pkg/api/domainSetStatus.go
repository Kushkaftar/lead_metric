package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func (h *Handler) setStatus(g *gin.Context) {

	g.JSON(http.StatusOK, gin.H{
		"success": true,
		"domains": "MOCK",
		"status":  "watch",
	})
}
