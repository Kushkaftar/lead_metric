package main

import (
	"flag"
	"fmt"
	"go.uber.org/zap"
	"lead_metric/internal/service"
	"lead_metric/pkg/api"
	"lead_metric/pkg/config"
	"lead_metric/pkg/logger"
	"os"
)

func main() {
	var logg *zap.Logger

	configFile := flag.String("cfg", "dev", "-cfg nameFile")

	flagMessage := "\"-log prod\" for write logs to file use prod"
	loggerFlag := flag.String("log", "", flagMessage)
	flag.Parse()

	switch *loggerFlag {
	case "prod":
		logg = logger.NewLogger(true)
		return
	default:
		logg = logger.NewLogger(false)
	}

	path := fmt.Sprintf("configs/%s.yaml", *configFile)

	_, err := os.Stat(path)
	if err != nil {
		logg.Fatal("configuration file not found", zap.Error(err))
	}

	c, err := config.NewConfig(*configFile, "configs")
	if err != nil {
		logg.Fatal("application exited with an error", zap.Error(err))
	}

	serv := service.NewApiMethods()
	api.Start(c, serv, logg)

}
