package server

import (
	"context"
	"fmt"
	"net/http"
)

type Server struct {
	httpServer *http.Server
}

func (s *Server) Run(port string, handler http.Handler) error {
	bindAddr := fmt.Sprintf(":%s", port)

	s.httpServer = &http.Server{
		Addr:    bindAddr,
		Handler: handler,
	}

	return s.httpServer.ListenAndServe()
}

func (s Server) Shutdown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}
