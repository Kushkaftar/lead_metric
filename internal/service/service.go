package service

type Service struct {
}

func newService() *Service {
	return &Service{}
}

type Domain interface {
	GetDomains()
	SetStatus()
}

type ApiMethod struct {
	Domain
}

func NewApiMethods() *ApiMethod {
	return &ApiMethod{
		Domain: newService(),
	}
}
